<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/form.css">
    <title>Document</title>
</head>
<body>
<!--<div id="result">
    <p>Сообщение улетело</p>
</div>-->
<!--<div id="form" class="form">
<form action='send.php' method='post' >
    <legend>Форма отбратной связи</legend>
  <p> Ваше имя:</p><label>
        <input type='text' name='fnm' value='' required="required">
    </label>
    <p> E-Mail:</p><label>
        <input type='email' name='email' value='' required="required">
    </label>
    <p>Телефон:</p>  +7<label>
        <input type="tel" name="tel" pattern="[0-9]{10}" required="required" maxlength="10">
    </label>
    <p> Календарь:</p><label>
        <input type='date' name='date' value='' required="required">
    </label>
    <p> Сообщение:</p><label>
        <textarea name='text' required="required"></textarea>
    </label>

    <input type='hidden' name='hidden' value=''>
    <p><input type='submit' value='отправить' name='submit'></p>
</form>
</div>-->
<!--Форма с бутстрапом-->
<div class="container">
    <div class="row">
    <div class="col-md-3">
    </div>
    <div class="col-md-5">

        <div id="result">
            <p>Сообщение улетело</p>
        </div>

        <div class="border border-dark rounded pl-3 pr-3 mt-2 mb-2"  id="form">

            <form action='send.php' method='post'>

        <div class="form-group">
            <label for="exampleInputName1">Name</label>
            <input type="text" class="form-control" id="exampleInputName1" placeholder="Enter Name" name="fnm" required>
            <small id="NameHelp" class="form-text text-muted">Введите имя.</small>
        </div>

        <div class="form-group">
            <label for="exampleInputEmail1">Email address</label>
            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                   placeholder="Enter email" name='email' required>
            <small id="emailHelp" class="form-text text-muted">Введите e-mail.</small>
        </div>

        <div class="form-group">
            <label for="exampleInputTel1">Phone</label>
            <input type="tel" class="form-control phone_mask" id="exampleInputTel1" placeholder="Enter phone"
                   name="tel" required>
            <small id="phoneHelp" class="form-text text-muted">Введите телефон.</small>
        </div>

        <div class="form-group">
            <label for="exampleInputCal1">Date</label>
            <input type="date" class="form-control" id="exampleInputCal1" placeholder="Enter Date" name='date' required>
            <small id="DateHelp" class="form-text text-muted">Выберите дата.</small>
        </div>

        <div class="form-group">
            <label for="exampleFormControlTextarea1">Message</label>
            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name='text' required></textarea>
        </div>

        <?php
        $root = $_SERVER['DOCUMENT_ROOT'];
        $file = fopen($root.'/mailCounter/counter.txt', 'r');
        $counter = file_get_contents($root.'/mailCounter/counter.txt');
        fclose($file);
        ?>

        <input type='hidden' name='hidden' value='<?php echo $counter;?>'>


        <button type="submit" class="btn btn-primary mb-2" value="submit" name="submit">Отправить</button>
    </form>
        </div>
    </div>
    <div class="col-md-3">
    </div>
    </div>
</div>

<script src="js/http_ajax.googleapis.com_ajax_libs_jquery_2.1.1_jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/form.js"></script>
<script src="js/jquery.maskedInputPhone.min.js"></script>
<!--<script src="js/jquery.inputMailMask.min.js"></script>-->
</body>
</html>
<?php
/*var_dump($counter);
if (isset($_POST['fnm'])) {
    var_dump($_POST);
}else{
    echo "Сообщение не улетело :(";
}*/