$(function () {
    $(".phone_mask").mask("+7(999)999-99-99");
//    $("#email-mask").inputmask("email");
    let good = document.getElementById("result");
    $(document).on("submit", 'form', function(e) {
        e.preventDefault();
       // e.stopPropagation();
        $.ajax({
            url: $(this).attr('action'),
            type: $(this).attr('method'),
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function () {
                $('form').hide(3000);
                //$('form').fadeOut(2000);
                $(good).show(3000);
                //$(good).fadeIn(2000);


            },
        });
    });
});